using Deimos.Areas.Identity.Data;
using Deimos.Components;
using Deimos.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("DeimosContextConnection") ?? throw new InvalidOperationException("Connection string 'DeimosContextConnection' not found.");

builder.Services.AddDbContext<DeimosContext>(options => options.UseSqlServer(connectionString));

builder.Services.AddDefaultIdentity<DeimosUser>(options => options.SignIn.RequireConfirmedAccount = true).AddEntityFrameworkStores<DeimosContext>();

// Add services to the container.
builder.Services.AddRazorComponents()
    .AddInteractiveServerComponents();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error", createScopeForErrors: true);
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseAntiforgery();

app.MapRazorComponents<App>()
    .AddInteractiveServerRenderMode();

app.Run();