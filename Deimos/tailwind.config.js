/** @type {import('tailwindcss').Config} */
module.exports = {
  // other options
  plugins: [
    require('flowbite/plugin')
  ],
  content: [
    // other files...
    "./**/*.{razor,html,cshtml}",
    "./node_modules/flowbite/**/*.js"
  ],
}
